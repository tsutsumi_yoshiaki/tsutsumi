﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
    public prefab prefab;
    string n;


    // ボタンが押された場合、今回呼び出される関数
    public void OnClick()
    {

        if (prefab.change >= 5)
            Application.Quit();
        else
            SceneManager.LoadScene("2backtask");
    }
    
}