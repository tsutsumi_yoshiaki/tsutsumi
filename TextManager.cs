﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;  // 追加しましょう


public class TextManager : MonoBehaviour
{
    public prefab prefab;
    public float resalt = 0;
    float resalt1=0;
    float resalt2 = 0;
    float resalt3 = 0;
    float resalt4 = 0;
    public GameObject text_object = null;
    // Start is called before the first frame update
    void Start()
    {

        Text score_text = text_object.GetComponent<Text>();
        switch (prefab.change)
        {
            
            case 1:
                score_text.text = ("①.1つ前に表示された記号と違う記号を押してください。");
                break;
            case 2:
                score_text.text = ("②.2つ前に表示された数字と違う数字を押してください。");
                resalt1 = resalt;
                resalt = 0;
                break;
            case 3:
                score_text.text = ("③.2つ前に表示された記号と違う記号があれば×。すべて同じであれば〇を押してください。");
                resalt2 = resalt;
                resalt = 0;
                break;
            case 4:
                score_text.text = ("④.6つの画像の中から1つだけ違う画像を探して押してください。");
                resalt3 = resalt;
                resalt = 0;
                break;
            case 5:
                resalt4 = resalt;
                score_text.text = ("結果\n①:記号 1バックタスク"+ resalt1/40 + "\n②:数字2バックタスク"+ resalt2/40 + "\n③:記号2バックタスク"+ resalt3/40 + "\n④:サーチタスク"+ resalt4);
                
                
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
