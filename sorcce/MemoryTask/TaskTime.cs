﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security;
using System.Security.Permissions;
using UnityEngine;

using UnityEngine.UI;

//構造体
public struct ColorID
{
    public int id;
    public string name;
    public bool flag;
}


public class TaskTime : MonoBehaviour
{
    private GameObject Timer;
    private GameObject Box;
    public static Image image;
    public static Sprite sprite;

    [SerializeField] GameObject Task;
    [SerializeField] GameObject Allow;
    [SerializeField] GameObject Task1;
    [SerializeField] GameObject Task2;
    [SerializeField] GameObject Task3;
    [SerializeField] GameObject Task4;
    [SerializeField] GameObject Search;
    [SerializeField] GameObject Search1;
    [SerializeField] GameObject Search2;
    [SerializeField] GameObject Search3;
    [SerializeField] GameObject Search4;
    [SerializeField] GameObject Search5;
    [SerializeField] GameObject Search6;

    float timekp = 0;
    bool count = true;
    public static byte flag = 0;
    byte delay;
    ColorID[] CID = new ColorID[7];
    ColorID[] CID_s = new ColorID[7];
    int lap = 0;
    int hogehoge = 0;
    int select;

    // Start is called before the first frame update
    // Use this for initialization
    void Start()
    {
        Timer = GameObject.Find("Timer");
        Task = GameObject.Find("task");
        Search = GameObject.Find("search");
        timekp = TimerTime.seconds;
        Task.SetActive(false);
        Search.SetActive(false);
        flag = 1;

        for (int i = 0; i < 7; i++)
        {
            CID[i].id = i;
            CID[i].flag = true;
        }
        CID[0].name = "black";
        CID[1].name = "blue";
        CID[2].name = "Magenta";
        CID[3].name = "red";
        CID[4].name = "white";
        CID[5].name = "yellow";
        CID[6].name = "yellowish-brown";
        for (int i = 0; i < 7; i++)
        {
            CID_s[i].id = i;
            CID_s[i].flag = true;
            CID_s[i].name = CID[i].name + "_s";
        }


    }


    // Update is called once per frame
    void Update()
    {
        if (TimerTime.seconds - timekp >= 0.6 && flag == 1)
        {
            timekp = TimerTime.seconds;
            Allow.SetActive(false);
            Task.SetActive(true);
            Task1 = GameObject.Find("task1");
            taskrotation(Task1, 0);
            color(Task1, CID);
            Task2 = GameObject.Find("task2");
            taskrotation(Task2, 0);
            color(Task2, CID);
            Task3 = GameObject.Find("task3");
            taskrotation(Task3, 0);
            color(Task3, CID);
            Task4 = GameObject.Find("task4");
            taskrotation(Task4, 0);
            color(Task4, CID);

            flag++;


        }
        if (TimerTime.seconds - timekp >= 0.1 && flag == 2)
        {

            timekp = TimerTime.seconds;

            switch (UnityEngine.Random.Range(0, 3))
            {
                case 0:
                    taskrotation(Task1, 1);
                    break;
                case 1:
                    taskrotation(Task2, 1);
                    break;
                case 2:
                    taskrotation(Task3, 1);
                    break;
                case 3:
                    taskrotation(Task4, 1);
                    break;
            }
            Task.SetActive(false);
            flag++;

        }
        if (TimerTime.seconds - timekp >= 0.9 && flag == 3)
        {
            timekp = TimerTime.seconds;
            if (UnityEngine.Random.Range(0, 2) == 0)
            {
                Task.SetActive(true);
            }
            else
            {
                Search.SetActive(true);

                Search1 = GameObject.Find("search1");
                color(Search1, CID_s);
                Search2 = GameObject.Find("search2");
                color(Search2, CID_s);
                Search3 = GameObject.Find("search3");
                color(Search3, CID_s);
                Search4 = GameObject.Find("search4");
                color(Search4, CID_s);
                Search5 = GameObject.Find("search5");
                color(Search5, CID_s);
                Search6 = GameObject.Find("search6");
                color(Search6, CID_s);
                hogehoge = UnityEngine.Random.Range(0, 6);
                searchrotation(Search1, hogehoge, 0);
                searchrotation(Search2, hogehoge, 1);
                searchrotation(Search3, hogehoge, 2);
                searchrotation(Search4, hogehoge, 3);
                searchrotation(Search5, hogehoge, 4);
                searchrotation(Search6, hogehoge, 5);
            }
            flag++;
        }
        if (TimerTime.seconds - timekp >= 4 && flag == 4)
        {
            timekp = TimerTime.seconds;
            Task.SetActive(false);
            Search.SetActive(false);
            flag++;
        }
        if (flag == 5)
        {
            timekp = TimerTime.seconds;
            switch (UnityEngine.Random.Range(0, 2))
            {
                case 0:
                    delay = 1;
                    break;
                case 1:
                    delay = 2;
                    break;
                case 2:
                    delay = 3;
                    break;
            }

            flag = 6;
        }
        if (flag == 6)
        {
            if (TimerTime.seconds - timekp >= 0.6)
            {
                timekp = TimerTime.seconds;
                lap++;
                Restart();
                SearchRestart(Search1);
                SearchRestart(Search2);
                SearchRestart(Search3);
                SearchRestart(Search4);
                SearchRestart(Search5);
                SearchRestart(Search6);
                delay = 0;

            }
            if (delay == 2 && TimerTime.seconds - timekp >= 0.85)
            {
                timekp = TimerTime.seconds;
                lap++;
                Restart();
                SearchRestart(Search1);
                SearchRestart(Search2);
                SearchRestart(Search3);
                SearchRestart(Search4);
                SearchRestart(Search5);
                SearchRestart(Search6);
                delay = 0;
            }
            if (delay == 3 && TimerTime.seconds - timekp >= 1.1)
            {
                timekp = TimerTime.seconds;
                lap++;
                Restart();
                SearchRestart(Search1);
                SearchRestart(Search2);
                SearchRestart(Search3);
                SearchRestart(Search4);
                SearchRestart(Search5);
                SearchRestart(Search6);
                delay = 0;
            }
        }
        if (lap == 64)
        {
            Application.Quit();

        }

        if (Input.GetKey(KeyCode.Escape)) Application.Quit();
    }

    public void taskrotation(GameObject Task, int x)
    {

        Transform myTransform = Task.transform;
        Vector3 localAngle = myTransform.localEulerAngles;
        switch (UnityEngine.Random.Range(x, 3))
        {
            case 0:
                break;
            case 1:
                localAngle.z += 45.0f;
                break;
            case 2:
                localAngle.z += 90.0f;
                break;
            case 3:
                localAngle.z += 135.0f;
                break;

        }
        myTransform.localEulerAngles = localAngle;
    }

    public static void color(GameObject A, ColorID[] CID)
    {
        int i = 0;

        while (true)
        {
            i = UnityEngine.Random.Range(0, 7);
            if (CID[i].flag == true)
            {
                break;
            }
        }

        sprite = Resources.Load<Sprite>(CID[i].name);
        image = A.GetComponent<Image>();

        image.sprite = sprite;

        CID[i].flag = false;
    }

    public void Restart()
    {
        timekp = TimerTime.seconds;
        Allow.SetActive(true);
        Task.SetActive(false);
        Search.SetActive(false);
        flag = 1;
        for (int i = 0; i < 7; i++)
        {
            CID[i].flag = true;
            CID_s[i].flag = true;
        }

    }

    public void searchrotation(GameObject A, int x, int y)
    {
        Transform myTransform = A.transform;
        Vector3 localAngle = myTransform.localEulerAngles;

        if (x == y)
        {
            if (UnityEngine.Random.Range(0, 2) == 0)
            {
                localAngle.z += 180.0f;
            }
        }
        else
        {
            if (UnityEngine.Random.Range(0, 2) == 0)
            {
                localAngle.z += 90.0f;
            }
            else
            {
                localAngle.z += 270.0f;
            }
        }

        myTransform.localEulerAngles = localAngle;
    }

    public void SearchRestart(GameObject A)
    {
        Transform myTransform = A.transform;
        Vector3 localAngle = myTransform.localEulerAngles;
        localAngle.z = 0.0f;
        myTransform.localEulerAngles = localAngle;

    }
   

}



