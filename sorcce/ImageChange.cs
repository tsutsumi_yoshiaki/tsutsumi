﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;


public class ImageChange : MonoBehaviour
{
    GameObject Timer;

    private Image image;
    private Sprite sprite;
    int n;
    int timekp;
    string s;
    bool count=true;
    int flag=0;

    // Use this for initialization
    void Start()
    {
        Timer = GameObject.Find("Timer");
        timekp = (int)TimerTime.seconds;
    }

    // Update is called once per frame
    void Update()
    {

        if ((int)TimerTime.seconds-timekp>=1 && flag==0 )
        {
            timekp = (int)TimerTime.seconds;
            sprite = Resources.Load<Sprite>("red");

            image = this.GetComponent<Image>();
            image.sprite = sprite;
            flag++;


        }
        if ((int)TimerTime.seconds - timekp >=2 && flag == 1)
        {
            timekp = (int)TimerTime.seconds;
            image = this.GetComponent<Image>();

            Destroy(image);
            flag++;
        }

    }
}
